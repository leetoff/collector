Установка docker и docker-compose на Ubuntu Linux
---

* sudo apt-get install \ apt-transport-https \ ca-certificates \ curl \ gnupg-agent \ software-properties-common
* curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
* sudo apt-key fingerprint 0EBFCD88
* sudo add-apt-repository \ "deb [arch=amd64] https://download.docker.com/linux/ubuntu \ $(lsb_release -cs) \ stable"
* sudo apt-get update
* sudo apt-get install docker-ce docker-ce-cli containerd.io
* sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
* sudo chmod +x /usr/local/bin/docker-compose

Установка для Windows
---

###### Вариант №1
* Скачать: https://download.docker.com/win/stable/DockerToolbox.exe
* Установить
* Запустить Docker Quickstart Terminal, управление происходит через него. Команды те же, что и на Ubuntu только sudo
  Обращение к контейнерам по адресу указанному в Docker Quickstart Terminal
###### Вариант №2
* Скачать: https://hub.docker.com/editions/community/docker-ce-desktop-windows
* Всё то же самое, только управление через Power Shell

###### Примечание:
* Вариант установки №1 предпочтительней
* Папка с проектом должна находиться в домашней директории пользователя C:\Users\username для того, чтобы докер
  смог её увидеть и примонтировать
  
Запуск
---
#### Для запуска нужно перейти в корневую папку проекта и выполнить
1. sudo docker-compose up -d
2. sudo docker exec -it collector_php composer install --prefer-dist
3. sudo docker exec -it collector_php php init  
4. sudo docker exec -it collector_php php yii migrate

* В случае изменения конфигурации докера нужно добавить флаг --build  
sudo docker-compose up -d --build

* Выключить всё:  
sudo docker-compose down

Запуск консольного приложения
---
1. sudo docker exec -it collector_php php yii collector/collect

    После чего появится таблица с собранной информацией, которую можно будет просмотреть в БД.
<?php

namespace Db\Infrastructure\Service;

use Db\Domain\Service\TransactionInterface;
use Yii;
use yii\db\Transaction;

class TransactionYiiService implements TransactionInterface
{
    /** @var Transaction active transaction */
    private $transaction;

    /**
     * @inheritDoc
     */
    public function begin(): void
    {
        $this->transaction = Yii::$app->db->beginTransaction();
    }

    /**
     * @inheritDoc
     */
    public function commit(): void
    {
        $this->transaction->commit();
    }

    /**
     * @inheritDoc
     */
    public function rollback(): void
    {
        $this->transaction->rollBack();
    }
}

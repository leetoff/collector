<?php


namespace Db\Infrastructure\Schema;


use Db\Domain\Schema\TableSchemaInterface;
use Yii;
use yii\db\ColumnSchemaBuilder;

class TableSchema implements TableSchemaInterface
{

    /**
     * @inheritDoc
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\Exception
     */
    public function createTable(string $table, array $columns, string $options = null): void
    {
        Yii::$app->db->createCommand()->createTable($table, $columns, $options)->execute();
        foreach ($columns as $column => $type) {
            if ($type instanceof ColumnSchemaBuilder && $type->comment !== null) {
                Yii::$app->db->createCommand()->addCommentOnColumn($table, $column, $type->comment)->execute();
            }
        }
    }
}

<?php

namespace Db\Domain\Schema;

interface TableSchemaInterface
{
    /**
     * @param string $table
     * @param array $columns
     * @param string|null $options
     */
    public function createTable(string $table, array $columns, string $options = null): void;
}

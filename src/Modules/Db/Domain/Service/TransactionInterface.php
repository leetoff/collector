<?php


namespace Db\Domain\Service;


interface TransactionInterface
{
    /**
     * Begins SQL transaction
     */
    public function begin(): void;

    /**
     * Commits SQL transaction
     */
    public function commit(): void;

    /**
     * Rollbacks SQL transaction
     */
    public function rollback(): void;
}

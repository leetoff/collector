<?php

namespace RestClient\Infrastructure\Manager;

use RestClient\Domain\Contract\RestResponseInterface;
use yii\httpclient\Exception;
use yii\httpclient\Response;

class RestResponseManager implements RestResponseInterface
{
    /** @var Response */
    private $response;

    /**
     * RestResponseManager constructor.
     *
     * @param Response $response
     */
    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @inheritDoc
     */
    public function getBody(): string
    {
        return $this->response->getContent();
    }

    /**
     * @inheritDoc
     */
    public function getAllHeaders(): array
    {
        return $this->response->getHeaders()->toArray();
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function getStatusCode(): string
    {
        return $this->response->getStatusCode();
    }

    /**
     * @inheritDoc
     */
    public function haveError(): bool
    {
        return !$this->response->isOk;
    }
}

<?php

namespace RestClient\Infrastructure\Manager\Mock;

use RestClient\Domain\Contract\RestRequestInterface;
use RestClient\Domain\Contract\RestResponseInterface;

class RestRequestMockManager implements RestRequestInterface
{
    /**
     * @inheritDoc
     */
    public function execute(): RestResponseInterface
    {
        return new RestResponseMockManager();
    }

    /**
     * @inheritDoc
     */
    public function setBasicAuth(string $username, string $password): void
    {

    }

    /**
     * @inheritDoc
     */
    public function setRequestUrl(string $requestUrl): void
    {

    }

    /**
     * @inheritDoc
     */
    public function setRequestMethod(string $methodName): void
    {

    }

    /**
     * @inheritDoc
     */
    public function setRequestTimeout(int $timeout): void
    {

    }

    /**
     * @inheritDoc
     */
    public function setQueryParameter(string $name, string $value): void
    {

    }

    /**
     * @inheritDoc
     */
    public function setRequestBody(string $body): void
    {

    }

    /**
     * @inheritDoc
     */
    public function setStringParameter(string $name, string $value): void
    {

    }

    /**
     * @inheritDoc
     */
    public function setRequestHeader(string $name, string $value): void
    {

    }

    /**
     * @inheritDoc
     */
    public function getRequestUrl(): string
    {
        return '';
    }

    /**
     * @inheritDoc
     */
    public function getRequestBody(): string
    {
        return '';
    }

    /**
     * @inheritDoc
     */
    public function getRequestHeaders(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getRequestMethod(): string
    {
        return 'GET';
    }
}

<?php

namespace RestClient\Infrastructure\Manager\Mock;

use RestClient\Domain\Contract\RestResponseInterface;

class RestResponseMockManager implements RestResponseInterface
{
    /**
     * @inheritDoc
     */
    public function getBody(): string
    {
        return file_get_contents(\Yii::getAlias('@mockdata') . '/api_response_body.json');
    }

    /**
     * @inheritDoc
     */
    public function getAllHeaders(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getStatusCode(): string
    {
        return '200';
    }

    /**
     * @inheritDoc
     */
    public function haveError(): bool
    {
        return false;
    }
}

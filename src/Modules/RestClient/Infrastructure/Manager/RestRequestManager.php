<?php

namespace RestClient\Infrastructure\Manager;

use RestClient\Domain\Contract\RestRequestInterface;
use RestClient\Domain\Contract\RestResponseInterface;
use yii\base\InvalidConfigException;
use yii\httpclient\Client;
use yii\httpclient\Exception;
use yii\httpclient\Request;

class RestRequestManager implements RestRequestInterface
{
    /** @var Request */
    private Request $request;

    /**
     * RestRequestManager constructor.
     *
     * @throws InvalidConfigException
     */
    public function __construct()
    {
        $this->request = (new Client())->createRequest();
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function execute(): RestResponseInterface
    {
        $response = $this->request->send();
        return new RestResponseManager($response);
    }

    /**
     * @inheritDoc
     */
    public function setBasicAuth(string $username, string $password): void
    {
        $encryptedString = 'Basic ' . base64_encode($username . ':' . $password);
        $this->setRequestHeader('Authorization', $encryptedString);
    }

    /**
     * @inheritDoc
     */
    public function setRequestUrl(string $requestUrl): void
    {
        $url = $this->request->getUrl();
        if (is_array($url)) {
            $url[0] = $requestUrl;
        } else {
            $url = $requestUrl;
        }
        $this->request->setUrl($url);
    }

    /**
     * @inheritDoc
     */
    public function setRequestMethod(string $methodName): void
    {
        $this->request->setMethod($methodName);
    }

    /**
     * @inheritDoc
     */
    public function setRequestTimeout(int $timeout): void
    {
        $this->request->setOptions(['timeout' => $timeout]);
    }

    /**
     * @inheritDoc
     */
    public function setQueryParameter(string $name, string $value): void
    {
        $newUrl = $this->request->getUrl();
        $newUrl = (array)$newUrl;
        $newUrl[$name] = $value;
        $this->request->setUrl($newUrl);
    }

    /**
     * @inheritDoc
     */
    public function setRequestBody(string $body): void
    {
        $this->request->setContent($body);
    }

    /**
     * @inheritDoc
     */
    public function setStringParameter(string $name, string $value): void
    {
        $newUrl = $this->request->getUrl();
        $newUrl[$name] = $name;
        $this->request->setUrl($newUrl);
    }

    /**
     * @inheritDoc
     */
    public function setRequestHeader(string $name, string $value): void
    {
        $this->request->getHeaders()->set($name, $value);
    }

    /**
     * @inheritDoc
     */
    public function getRequestUrl(): string
    {
        return $this->request->getFullUrl();
    }

    /**
     * @inheritDoc
     */
    public function getRequestBody(): string
    {
        return $this->request->getContent();
    }

    /**
     * @inheritDoc
     */
    public function getRequestHeaders(): array
    {
        return $this->request->getHeaders()->toArray();
    }

    /**
     * @inheritDoc
     */
    public function getRequestMethod(): string
    {
        return $this->request->getMethod();
    }
}

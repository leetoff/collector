<?php

namespace RestClient\Domain\Contract;

interface RestResponseInterface
{
    /**
     * Gets the content of the response body.
     *
     * @return string
     */
    public function getBody(): string;

    /**
     * Returns array with all headers contained in the response.
     *
     * @return array
     */
    public function getAllHeaders(): array;

    /**
     * Gets the numeric HTTP status code.
     *
     * @return string
     */
    public function getStatusCode(): string;

    /**
     * Indicates if there was an error during the rest transaction.
     *
     * @return bool
     */
    public function haveError(): bool;
}

<?php

namespace RestClient\Domain\Contract;

interface RestRequestInterface
{
    /**
     * Sends the request.
     *
     * @return RestResponseInterface
     */
    public function execute(): RestResponseInterface;

    /**
     * Sets $userName with $userPass to authorize on web service if resource applies basic authorization.
     *
     * @param string $username
     * @param string $password
     */
    public function setBasicAuth(string $username, string $password): void;

    /**
     * Sets the request URL.
     *
     * @param string $requestUrl
     */
    public function setRequestUrl(string $requestUrl): void;

    /**
     * Sets request method such as GET, PUT etc.
     *
     * @param string $methodName
     */
    public function setRequestMethod(string $methodName): void;

    /**
     * Sets the amount of time in seconds to wait for a response
     * from the web service provider before the request times out.
     *
     * @param int $timeout
     */
    public function setRequestTimeout(int $timeout): void;

    /**
     * Appends a parameter to the end of the request URL with the form name=value.
     *
     * @param string $name
     * @param string $value
     */
    public function setQueryParameter(string $name, string $value): void;

    /**
     * Sets the body content to send to the web service provider when using PUT or POST request methods.
     *
     * @param string $body
     */
    public function setRequestBody(string $body): void;

    /**
     * Set a request variable with the specified name to the specified value.
     *
     * @param string $name
     * @param string $value
     */
    public function setStringParameter(string $name, string $value): void;

    /**
     * Set an HTTP header in the request to the specified value.
     *
     * @param string $name
     * @param string $value
     */
    public function setRequestHeader(string $name, string $value): void;

    /**
     * Returns request url with parameters.
     *
     * @return string
     */
    public function getRequestUrl(): string;

    /**
     * Returns request content.
     *
     * @return string
     */
    public function getRequestBody(): string;

    /**
     * Returns all request headers.
     *
     * @return array
     */
    public function getRequestHeaders(): array;

    /**
     * Returns request method.
     *
     * @return string
     */
    public function getRequestMethod(): string;
}

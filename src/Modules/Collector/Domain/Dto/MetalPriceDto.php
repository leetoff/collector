<?php

namespace Collector\Domain\Dto;

class MetalPriceDto
{
    /** @var string */
    public $metalType;

    /** @var float */
    public $price;
}

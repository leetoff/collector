<?php

namespace Collector\Domain\Dto;

class SupplierMetalDto
{
    /** @var int */
    public $supplierId;

    /** @var string */
    public $metalType;
}

<?php

namespace Collector\Domain\Repository;

use Collector\Domain\Aggregate\SupplyInfo;

interface SupplyInfoRepositoryInterface
{
    /**
     * @param string $tableName
     * @param SupplyInfo[] $supplyInfos
     * @return bool
     */
    public function batchInsert(string $tableName, array $supplyInfos): bool;
}

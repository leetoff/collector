<?php

namespace Collector\Domain\Repository;

use Collector\Domain\Entity\Supplier;

interface SupplierRepositoryInterface
{
    /**
     * @return Supplier[]
     */
    public function findAll(): array;
}

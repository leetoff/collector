<?php

namespace Collector\Domain\Service;

interface CreateCollectTableServiceInterface
{
    /**
     * Creates table for collect. Returns table name;
     *
     * @return string
     */
    public function create(): string;
}

<?php

namespace Collector\Domain\ValueObject;

class CollectTableColumns
{
    /** @var string */
    public const COLUMN_ID = 'id';

    /** @var string */
    public const COLUMN_SUPPLIER_ID = 'supplier_id';

    /** @var string */
    public const COLUMN_NAME = 'name';

    /** @var string */
    public const COLUMN_METAL_TYPE = 'metal_type';

    /** @var string */
    public const COLUMN_PRICE = 'price';
}

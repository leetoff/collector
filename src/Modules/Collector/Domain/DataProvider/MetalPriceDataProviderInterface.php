<?php

namespace Collector\Domain\DataProvider;

use Collector\Domain\Dto\MetalPriceDto;

interface MetalPriceDataProviderInterface
{
    /**
     * @return MetalPriceDto[]
     */
    public function getMetalPrices(): array;
}

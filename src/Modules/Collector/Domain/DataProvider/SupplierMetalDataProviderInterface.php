<?php


namespace Collector\Domain\DataProvider;

use Collector\Domain\Dto\SupplierMetalDto;

interface SupplierMetalDataProviderInterface
{
    /**
     * @param int $supplierId
     *
     * @return SupplierMetalDto | null
     */
    public function getBySupplierId(int $supplierId): ?SupplierMetalDto;
}

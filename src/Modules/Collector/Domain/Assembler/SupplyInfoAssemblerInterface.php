<?php

namespace Collector\Domain\Assembler;

use Collector\Domain\Aggregate\SupplyInfo;

interface SupplyInfoAssemblerInterface
{
    /**
     * Assemble array of SupplyInfo for collect all info about supply.
     *
     * @return SupplyInfo[]
     */
    public function assemble(): array;
}

<?php

namespace Collector\Domain\Aggregate;

use Collector\Domain\Dto\MetalPriceDto;
use Collector\Domain\Dto\SupplierMetalDto;
use Collector\Domain\Entity\Supplier;

class SupplyInfo
{
    private Supplier $supplier;
    private SupplierMetalDto $supplierMetal;
    private MetalPriceDto $metalPrice;

    public function __construct(Supplier $supplier, SupplierMetalDto $supplierMetal, MetalPriceDto $metalPrice)
    {
        $this->supplier = $supplier;
        $this->supplierMetal = $supplierMetal;
        $this->metalPrice = $metalPrice;
    }

    public function getSupplierId(): ?int
    {
        return $this->supplier->supplier_id ?? $this->supplierMetal->supplierId;
    }

    public function getSupplierName(): ?string
    {
        return $this->supplier->name;
    }

    public function getMetalType(): ?string
    {
        return $this->supplierMetal->metalType ?? $this->metalPrice->metalType;
    }

    public function getPrice(): ?float
    {
        return $this->metalPrice->price;
    }
}

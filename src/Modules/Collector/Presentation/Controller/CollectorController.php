<?php

namespace Collector\Presentation\Controller;

use Collector\Application\Command\CollectSupplyInfoInDbCommand;
use yii\console\Controller;
use yii\console\ExitCode;

class CollectorController extends Controller
{
    private CollectSupplyInfoInDbCommand $collectSupplyInfoInDbCommand;

    public function __construct(
        $id,
        $module,
        CollectSupplyInfoInDbCommand $collectSupplyInfoInDbCommand,
        $config = []
    ) {
        $this->collectSupplyInfoInDbCommand = $collectSupplyInfoInDbCommand;
        parent::__construct($id, $module, $config);
    }

    public function actionCollect(): int
    {
        echo 'Table with supply info created successfully: "'
            . $this->collectSupplyInfoInDbCommand->execute()
            . '"'
            . PHP_EOL;
        return ExitCode::OK;
    }
}

<?php

namespace Collector\Infrastructure\Repository;

use Collector\Domain\Entity\Supplier;
use Collector\Domain\Repository\SupplierRepositoryInterface;
use Core\Domain\Mapper\MapperInterface;
use yii\db\Query;

class SupplierRepository implements SupplierRepositoryInterface
{
    private MapperInterface $mapper;

    public function __construct(MapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @inheritDoc
     */
    public function findAll(): array
    {
        $source = (new Query())
            ->from(Supplier::TABLE_NAME)
            ->all();

        /** @var Supplier[] $suppliers */
        $suppliers = $this->mapper->mapItems($source, new Supplier);
        return $suppliers;
    }
}

<?php

namespace Collector\Infrastructure\Repository;

use Collector\Domain\Aggregate\SupplyInfo;
use Collector\Domain\Repository\SupplyInfoRepositoryInterface;
use Db\Domain\Service\TransactionInterface;
use Yii;
use yii\db\Exception;

class SupplyInfoRepository implements SupplyInfoRepositoryInterface
{
    private TransactionInterface $transaction;

    public function __construct(TransactionInterface $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @param string $tableName
     * @param array $supplyInfos
     * @return bool
     * @throws Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\NotSupportedException
     */
    public function batchInsert(string $tableName, array $supplyInfos): bool
    {
        $rows = [];
        foreach ($supplyInfos as $supplyInfo) {
            $rows[] = [
                'supplier_id' => $supplyInfo->getSupplierId(),
                'price' => $supplyInfo->getPrice(),
                'metal_type' => $supplyInfo->getMetalType(),
                'name' => $supplyInfo->getSupplierName()
            ];
        }
        // Для предотвращения ошибки, если объектов больше 65535
        // SQLSTATE[HY000]: General error: 7 number of parameters must be between 0 and 65535
        $chunkedRows = array_chunk($rows, 5000, true);
        $result = 0;
        $this->transaction->begin();
        $columns = array_keys(reset($rows));
        try {
            foreach ($chunkedRows as $rowsChunk) {
                $result += Yii::$app->db
                    ->createCommand()
                    ->batchInsert($tableName, $columns, $rowsChunk)
                    ->execute();
            }
            $this->transaction->commit();
        } catch (\Exception $exception) {
            $this->transaction->rollback();
            throw $exception;
        }
        return (bool) $result;
    }
}

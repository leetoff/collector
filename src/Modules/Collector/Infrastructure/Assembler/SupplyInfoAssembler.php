<?php

namespace Collector\Infrastructure\Assembler;

use Collector\Domain\Aggregate\SupplyInfo;
use Collector\Domain\Assembler\SupplyInfoAssemblerInterface;
use Collector\Domain\DataProvider\MetalPriceDataProviderInterface;
use Collector\Domain\DataProvider\SupplierMetalDataProviderInterface;
use Collector\Domain\Dto\MetalPriceDto;
use Collector\Domain\Dto\SupplierMetalDto;
use Collector\Domain\Repository\SupplierRepositoryInterface;
use Core\Domain\Helper\ArrayHelper;

class SupplyInfoAssembler implements SupplyInfoAssemblerInterface
{
    private SupplierRepositoryInterface $supplierRepository;
    private SupplierMetalDataProviderInterface $supplierMetalDataProvider;
    private MetalPriceDataProviderInterface $metalPriceDataProvider;

    public function __construct(
        SupplierRepositoryInterface $supplierRepository,
        SupplierMetalDataProviderInterface $supplierMetalDataProvider,
        MetalPriceDataProviderInterface $metalPriceDataProvider
    ) {
        $this->supplierRepository = $supplierRepository;
        $this->supplierMetalDataProvider = $supplierMetalDataProvider;
        $this->metalPriceDataProvider = $metalPriceDataProvider;
    }

    /**
     * @inheritDoc
     */
    public function assemble(): array
    {
        $suppliers = $this->supplierRepository->findAll();
        $metalPrices = $this->metalPriceDataProvider->getMetalPrices();
        $metalIndexedArray = array_flip(ArrayHelper::getColumn($metalPrices, 'metalType'));
        $supplyInfos = [];
        foreach ($suppliers as $supplier) {
            $supplierMetalDto = $this->supplierMetalDataProvider->getBySupplierId($supplier->supplier_id);
            if (is_null($supplierMetalDto)) {
                $supplierMetalDto = new SupplierMetalDto();
            }
            if (true === isset($metalIndexedArray[$supplierMetalDto->metalType])) {
                $metalPrice = $metalPrices[$metalIndexedArray[$supplierMetalDto->metalType]];
            } else {
                $metalPrice = new MetalPriceDto();
            }
            $supplyInfos[] = new SupplyInfo($supplier, $supplierMetalDto, $metalPrice);
        }
        return $supplyInfos;
    }
}

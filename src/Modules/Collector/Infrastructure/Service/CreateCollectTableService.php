<?php

namespace Collector\Infrastructure\Service;

use Collector\Domain\Service\CreateCollectTableServiceInterface;
use Collector\Domain\ValueObject\CollectTableColumns;
use Db\Domain\Schema\TableSchemaInterface;

class CreateCollectTableService implements CreateCollectTableServiceInterface
{
    private TableSchemaInterface $tableSchema;

    public function __construct(TableSchemaInterface $tableSchema) {
        $this->tableSchema = $tableSchema;
    }

    public function create(): string
    {
        $tableName = 'supply_info' . date("Y-m-d-H-i-s");
        $this->tableSchema->createTable($tableName, [
            CollectTableColumns::COLUMN_ID => 'pk',
            CollectTableColumns::COLUMN_SUPPLIER_ID => 'integer',
            CollectTableColumns::COLUMN_NAME => 'string',
            CollectTableColumns::COLUMN_METAL_TYPE => "ENUM('Gold', 'Silver', 'Platinum')",
            CollectTableColumns::COLUMN_PRICE => 'float',
        ]);
        return $tableName;
    }
}

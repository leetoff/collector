<?php

namespace Collector\Infrastructure\DataProvider;

use Collector\Domain\DataProvider\SupplierMetalDataProviderInterface;
use Collector\Domain\Dto\SupplierMetalDto;
use Core\Domain\Mapper\MapperInterface;
use RestClient\Domain\Contract\RestRequestInterface;

class SupplierMetalApiDataProvider implements SupplierMetalDataProviderInterface
{
    private RestRequestInterface $restRequest;
    private MapperInterface $mapper;

    public function __construct(RestRequestInterface $restRequest, MapperInterface $mapper)
    {
        $this->restRequest = $restRequest;
        $this->mapper = $mapper;
    }

    /**
     * @inheritDoc
     */
    public function getBySupplierId(int $supplierId): ?SupplierMetalDto
    {
        $this->restRequest->setBasicAuth('user', 'MTIzNDU2');
        $this->restRequest->setRequestUrl('http://api.test/v1/get-supplier-info/' . $supplierId);
        $this->restRequest->setRequestMethod('GET');
        $source = json_decode($this->restRequest->execute()->getBody(), true);
        if (false === isset($source[$supplierId]['data'])) {
            return null;
        }
        /** @var SupplierMetalDto $result */
        $result = $this->mapper->map($source[$supplierId]['data'], new SupplierMetalDto, [
            'supplierId' => 'supplier_id',
            'metalType' => 'metal_type'
        ]);
        return $result;
    }
}

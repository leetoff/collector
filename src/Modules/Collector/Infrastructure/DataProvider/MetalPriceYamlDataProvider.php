<?php

namespace Collector\Infrastructure\DataProvider;

use Collector\Domain\DataProvider\MetalPriceDataProviderInterface;
use Collector\Domain\Dto\MetalPriceDto;
use Core\Domain\Helper\YamlHelper;
use Core\Domain\Mapper\MapperInterface;

class MetalPriceYamlDataProvider implements MetalPriceDataProviderInterface
{
    private MapperInterface $mapper;

    public function __construct(MapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @inheritDoc
     */
    public function getMetalPrices(): array
    {
        $source = YamlHelper::fileToArray(\Yii::getAlias('@mockdata') . '/test.yml');

        $result = [];
        foreach ($source as $type => $price) {
            $result[] = $this->mapper->map(['metalType' => $type, 'price' => $price], new MetalPriceDto);
        }
        return $result;
    }
}

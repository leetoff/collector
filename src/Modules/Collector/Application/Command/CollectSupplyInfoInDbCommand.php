<?php

namespace Collector\Application\Command;

use Collector\Domain\Assembler\SupplyInfoAssemblerInterface;
use Collector\Domain\Exception\EmptyCollectDataException;
use Collector\Domain\Repository\SupplyInfoRepositoryInterface;
use Collector\Domain\Service\CreateCollectTableServiceInterface;

class CollectSupplyInfoInDbCommand
{
    private SupplyInfoAssemblerInterface $supplyInfoAssembler;
    private SupplyInfoRepositoryInterface $supplyInfoRepository;
    private CreateCollectTableServiceInterface $createCollectTableService;

    public function __construct(
        SupplyInfoAssemblerInterface $supplyInfoAssembler,
        SupplyInfoRepositoryInterface $supplyInfoRepository,
        CreateCollectTableServiceInterface $createCollectTableService
    ) {
        $this->supplyInfoRepository = $supplyInfoRepository;
        $this->supplyInfoAssembler = $supplyInfoAssembler;
        $this->createCollectTableService = $createCollectTableService;
    }

    /**
     * Collects data and inserts it into db table. Returns table name of collect table.
     *
     * @return string
     * @throws EmptyCollectDataException
     */
    public function execute(): string
    {
        $collectData = $this->supplyInfoAssembler->assemble();
        if (true === empty($collectData)) {
            throw new EmptyCollectDataException('Collect data is empty. Please, check source instances.');
        }
        $tableName = $this->createCollectTableService->create();
        $this->supplyInfoRepository->batchInsert($tableName, $collectData);
        return $tableName;
    }
}

<?php

namespace Core\Domain\Helper;

class YamlHelper
{
    /**
     * @param string $filePath
     * @return mixed[]
     */
    public static function fileToArray(string $filePath): array
    {
        return yaml_parse_file($filePath);
    }
}

<?php

namespace Core\Domain\Mapper;

interface MapperInterface
{
    /**
     * Maps source (array or object) to target object.
     * 1. Finds array of target properties names
     * 2. Finds source property value and set property of target object in cycle
     *
     * @param array|object $source
     * @param object|string $target class instance or class name of target object
     * @param array $config array in format:
     * - key - property name
     * - value - can be of 2 types:
     *    - callback - function which gets source and returns value
     *    - string - property name of source whose value need to set to the target
     * @return object
     */
    public function map($source, $target, $config = []): object;

    /**
     * Maps array of sources via self::map()
     *
     * @param array $sources
     * @param object|string $class class instance or class name of target objects
     * @param array $config
     * @return object[]
     */
    public function mapItems(array $sources, $class, $config = []): array;

    /**
     * Returns object attributes in array format, when keys are object properties
     *
     * @param object $source
     * @return array
     */
    public function toArray(object $source): array;

    /**
     * Returns object properties
     *
     * @param object $object
     * @return mixed[]
     */
    public function getProperties(object $object): array;
}

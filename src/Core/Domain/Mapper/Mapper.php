<?php

namespace Core\Domain\Mapper;

use Core\Domain\Exception\MapException;
use Yii;

/**
 * Class Mapper
 * @package Core\Domain\Mapper
 *
 */
class Mapper implements MapperInterface
{
    /**
     * @inheritDoc
     * @throws
     */
    public function map($source, $target, $config = []): object
    {
        if (is_array($source)) {
            $source = (object)$source;
        }

        if (!is_object($source)) {
            throw new MapException('The type of source is not supported.');
        }

        if (!is_object($target)) {
            $target = Yii::createObject($target);
        }

        $properties = $this->getProperties($target);
        foreach ($properties as $property) {
            $target->{$property} = $this->getValue($source, $property, $config);
        }

        return $target;
    }

    /**
     * @inheritDoc
     * @throws
     */
    public function mapItems(array $sources, $class, $config = []): array
    {
        if (is_object($class)) {
            $class = get_class($class);
        }

        return array_map(function ($source) use ($class, $config) {
            return $this->map($source, Yii::createObject($class), $config);
        }, $sources);
    }

    /**
     * Returns target properties, which need to fill.
     *
     * @inheritDoc
     */
    public function getProperties(object $object): array
    {
        return array_keys($this->toArray($object));
    }

    /**
     * @inheritDoc
     */
    public function toArray(object $source): array
    {
        $properties = $this->getObjectProperties($source);

        $values = array_map(static function($property) use ($source) {
            return $source->$property;
        }, $properties);

        return array_combine($properties, $values);
    }

    /**
     * @param object $entity
     * @return array of values of $properties
     */
    protected function getObjectProperties(object $entity): array
    {
        return array_keys(get_object_vars($entity));
    }

    /**
     * Returns property value by source value or callback result
     *
     * @param object $source
     * @param string $property
     * @param array $config
     * @return mixed|null
     */
    private function getValue(object $source, string $property, array $config)
    {
        if (isset($config[$property])) {
            if (is_callable($config[$property])) {
                $value = call_user_func($config[$property], $source);
            } elseif (is_string($config[$property])) {
                $property = $config[$property];
            }
        }

        $value = $value ?? ($source->$property ?? null);

        return $value;
    }
}

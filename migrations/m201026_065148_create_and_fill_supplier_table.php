<?php

use yii\db\Migration;

/**
 * Handles the creation of table `supplier`.
 */
class m201026_065148_create_and_fill_supplier_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('supplier', [
            'id' => $this->primaryKey(),
            'supplier_id' => $this->integer(),
            'name' => $this->string()
        ]);
        $this->insert('supplier', [
            'supplier_id' => 1,
            'name' => 'First Supplier'
        ]);
        $this->insert('supplier', [
            'supplier_id' => 2,
            'name' => 'Second Supplier'
        ]);
        $this->insert('supplier', [
            'supplier_id' => 3,
            'name' => 'Third Supplier'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('supplier');
    }
}

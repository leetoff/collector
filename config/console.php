<?php

namespace config;

use yii\caching\FileCache;
use yii\log\FileTarget;

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => require __DIR__ . '/aliases.php',
    'components' => [
        'cache' => [
            'class' => FileCache::class,
        ],
        'log' => [
            'targets' => [
                [
                    'class' => FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require __DIR__ . '/db.php',
    ],
    'container' => [
        'definitions' => require __DIR__ . '/di.php',
        'singletons' => require __DIR__ . '/singletons.php',
    ],
    'controllerMap' => require __DIR__ . '/controllers.php'
];

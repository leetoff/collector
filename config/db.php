<?php

namespace config;

use yii\db\Connection;

return [
    'class' => Connection::class,
    'dsn' => 'mysql:host=' . getenv('DB_DATABASE') . ';port=3306;dbname=' . getenv('DB_DATABASE'),
    'username' => getenv('DB_USER'),
    'password' => getenv('DB_PASSWORD'),
    'charset' => 'utf8',
];

<?php

namespace config;

return [
    '@bower' => '@vendor/bower-asset',
    '@npm'   => '@vendor/npm-asset',
    '@tests' => '@app/tests',
    '@Collector' => dirname(__DIR__) . '/src/Modules/Collector',
    '@Core' => dirname(__DIR__) . '/src/Core',
    '@RestClient' => dirname(__DIR__) . '/src/Modules/RestClient',
    '@mockdata' => dirname(__DIR__) . '/mockdata',
    '@Db' => dirname(__DIR__) . '/src/Modules/Db',
];

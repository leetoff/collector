<?php

namespace config;

use Db\Domain\Service\TransactionInterface;
use Db\Infrastructure\Service\TransactionYiiService;

return [
    TransactionInterface::class => TransactionYiiService::class
];
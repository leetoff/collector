<?php

namespace config;

use Collector\Presentation\Controller\CollectorController;

return [
    'collector' => CollectorController::class,
];

<?php

namespace config;

use Collector\Domain\Assembler\SupplyInfoAssemblerInterface;
use Collector\Domain\DataProvider\MetalPriceDataProviderInterface;
use Collector\Domain\DataProvider\SupplierMetalDataProviderInterface;
use Collector\Domain\Repository\SupplierRepositoryInterface;
use Collector\Domain\Repository\SupplyInfoRepositoryInterface;
use Collector\Domain\Service\CreateCollectTableServiceInterface;
use Collector\Infrastructure\Assembler\SupplyInfoAssembler;
use Collector\Infrastructure\DataProvider\MetalPriceYamlDataProvider;
use Collector\Infrastructure\DataProvider\SupplierMetalApiDataProvider;
use Collector\Infrastructure\Repository\SupplierRepository;
use Collector\Infrastructure\Repository\SupplyInfoRepository;
use Collector\Infrastructure\Service\CreateCollectTableService;
use Core\Domain\Mapper\MapperInterface;
use Core\Domain\Mapper\Mapper;
use Db\Domain\Schema\TableSchemaInterface;
use Db\Infrastructure\Schema\TableSchema;
use RestClient\Domain\Contract\RestRequestInterface;
use RestClient\Domain\Contract\RestResponseInterface;
use RestClient\Infrastructure\Manager\Mock\RestRequestMockManager;
use RestClient\Infrastructure\Manager\Mock\RestResponseMockManager;

return [
    //Core
    MapperInterface::class => Mapper::class,

    //Collector
    SupplierRepositoryInterface::class => SupplierRepository::class,
    MetalPriceDataProviderInterface::class => MetalPriceYamlDataProvider::class,
    SupplierMetalDataProviderInterface::class => SupplierMetalApiDataProvider::class,
    CreateCollectTableServiceInterface::class => CreateCollectTableService::class,
    SupplyInfoAssemblerInterface::class => SupplyInfoAssembler::class,
    SupplyInfoRepositoryInterface::class => SupplyInfoRepository::class,

    //Rest Client
    RestRequestInterface::class => RestRequestMockManager::class,
    RestResponseInterface::class => RestResponseMockManager::class,

    //Db
    TableSchemaInterface::class => TableSchema::class,
];
